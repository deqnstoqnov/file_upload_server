## What is this

This is a rest server which uploads files in parallel with up to 4 threads.
It spreads evenly the uploaded files on the server file system in a directory
which is passed as argument to the server. For each file the client is obliged
to calculate md5 sum. For each hex digit of the md5 sum we create a sub directory in the server.
Thus we will not have more than 16 directories in a directory and more than a
single file in a directory. This solves the problem of overloading the limitations of
any file system.

## How to start it
We assume you are in the root of the project. You have to pass a directory for server's main storage root.

```bash
mvn  exec:java -Dstorage_dir=/tmp
```

## How to use it
We upload as example the pom file of the project. We have calculated the md5sum before this.
We have added "sleep" header which will lead to that many seconds sleep in the servers thread.
This way we simulate large file work. Thus it will be easier to demonstrate the server uses at most 4 threads.
Set the sleep header to 60 and run 5 times one after the other this curl. The 5th response will be 429 Too many requests.

```bash
curl -v  -X PUT  --header "Content-Type: application/octet-stream" --header "sleep: 5" -d "@pom.xml" http://localhost:8080/app/files/29b3914ab53a497c8df4825936848652 
``` 
