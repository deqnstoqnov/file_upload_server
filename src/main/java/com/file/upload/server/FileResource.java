package com.file.upload.server;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Root resource (exposed at "/files" path)
 */
@Path("/files")
public class FileResource {

	private static final Semaphore semaphore = new Semaphore(4);
	private static final ExecutorService executor = Executors.newFixedThreadPool(4);

	/**
	 * Method handling HTTP GET requests. The returned object will be sent to the
	 * client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getIt() {
		return "Got it!";
	}

	@PUT
	@Path("/{md5}")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response uploadFile(@PathParam("md5") String md5, byte[] body, @HeaderParam("sleep") String sleep) {
		final CustomRunnable runnable = new CustomRunnable(md5, body, semaphore, sleep);

		// very important, we don't block
		if (semaphore.tryAcquire()) {
			executor.execute(runnable);
			return Response.ok().build();
		} else {
			return Response.status(Response.Status.TOO_MANY_REQUESTS).build();
		}
	}

}
