package com.file.upload.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Semaphore;

public class CustomRunnable implements Runnable {

	private String md5;
	private byte[] bytes;
	private Semaphore semaphore;
	private int sleep;

	public CustomRunnable(String md5, byte[] bytes, Semaphore semaphore, String sleep) {
		this.md5 = md5;
		this.bytes = bytes;
		this.semaphore = semaphore;
		this.sleep = Integer.parseInt(sleep);
	}

	@Override
	public void run() {
		try {
			writeFile(md5, bytes);
			Thread.sleep(sleep * 1000);
		} catch (Exception e) {
			writeError(md5);
		} finally {
			semaphore.release();
		}
	}

	private void writeError(String md5) {
		java.nio.file.Path path;
		try {
			path = createDirsByMD5(md5);
			final java.nio.file.Path file = Paths.get(path.toString(), "error");
			Files.write(file, "error".getBytes());
		} catch (IOException e) {
			// here we are dead, we may try write error file again
			e.printStackTrace();
		}
	}

	private String writeFile(String md5, byte[] bytes) throws IOException {
		final java.nio.file.Path path = createDirsByMD5(md5);
		final java.nio.file.Path file = Paths.get(path.toString(), "file");
		Files.write(file, bytes);
		return md5;
	}

	private java.nio.file.Path createDirsByMD5(String md5) throws IOException {
		final char[] charArray = md5.toCharArray();

		final StringBuilder builder = new StringBuilder();
		for (final char c : charArray) {
			builder.append(c).append("/");
		}
		builder.delete(builder.length() - 1, builder.length());
		final String dirStructure = builder.toString();
		final java.nio.file.Path path = Paths.get(System.getProperty("storage_dir"), dirStructure);
		return Files.createDirectories(path);
	}

}
